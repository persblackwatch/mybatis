package pers.xzp.mybatis.dao;

import org.apache.ibatis.annotations.Param;
import pers.xzp.mybatis.pojo.User;

import java.util.List;
import java.util.Map;

/**
 * @author : XZP
 * @version : 1.0
 * @date ：2020-09-14 22:51
 * @description : UserDao接口
 */
public interface UserMapper {
    List<User> queryAllUser();

    List<User> queryListUser(User user);

    /**
     * 接收多个参数
     *
     * @param id
     * @param username
     * @return
     */
    User queryUser2(@Param("id") int id, @Param("username") String username);

    /**
     * 接收一个参数，完成查询
     * 单个参数，可以不使用注解@Param绑定参数
     * @param id
     * @return
     */
    User queryUser3(int id);

    List<User> queryUser(User user);

    /**
     * 分页查询1
     *
     * @param map
     * @return
     */
    List<User> queryUserByLimit1(Map<String, Integer> map);

    /**
     * 分页查询2
     *
     * @return
     */
    List<User> queryUserByLimit2();
}
