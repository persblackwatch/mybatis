package pers.xzp.mybatis;


import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import pers.xzp.mybatis.dao.UserMapper;
import pers.xzp.mybatis.pojo.User;
import pers.xzp.mybatis.utils.SqlsessionUtils;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class UserTest
{
    private static SqlSession sqlSession = SqlsessionUtils.getSqlSession();

    /**
     * 动态sql 根据id和username模糊查询
     * 模糊查询不行  拼接出来的sql正常执行
     */
    @Test public void queryUser(){
        User user = new User();
        user.setId(41);
        user.setUsername("小");
        Assert.assertNotNull(sqlSession.getMapper(UserMapper.class).queryUser(user));
    }

    /**
     * 接收多个参数 完成查询
     */
    @Test public void queryUser2(){
        Assert.assertNotNull(sqlSession.getMapper(UserMapper.class).queryUser2(41,"十号"));
    }

    /**
     * 接收一个参数，完成查询
     * 单个参数，可以不使用注解@Param绑定参数
     */
    @Test public void queryUser3(){
        Assert.assertNotNull(sqlSession.getMapper(UserMapper.class).queryUser3(41));
    }

    /**
     * 查询所有记录
     */
    @Test public void queryAllUser(){
        Assert.assertNotNull(sqlSession.getMapper(UserMapper.class).queryAllUser());
    }

    /**
     * 根据多个id查询对应记录
     */
    @Test public void queryListUser(){
        User user = new User();
        List<Integer> idList = Arrays.asList(41,42,51,55);
        user.setIds(idList);
        Assert.assertNotNull(sqlSession.getMapper(UserMapper.class).queryListUser(user));
    }

    /**
     * limit分页测试1
     */
    @Test public void queryUserByLimit1(){
        HashMap<String, Integer> map = new HashMap<>();
        map.put("pageIndex",0);
        map.put("pageSize",2);
        Assert.assertNotNull(sqlSession.getMapper(UserMapper.class).queryUserByLimit1(map));
    }

    /**
     * limit分页测试2
     * 基于java层次的分页
     * 简单而言就是RowBounds对象代替sql 的limit
     */
    @Test public void queryUserByLimit2(){
        //sqlSession.selectList(String statement)：statement指向xml中id
        sqlSession.selectList("queryUserByLimit2",null,new RowBounds(1,2));
    }


    @After public void close(){
        if (sqlSession != null) {
            sqlSession.close();
        }
    }

    /**
     * log4j日志应用测试
     */
    @Test public void logTest(){
        Logger logger = Logger.getLogger(UserTest.class);
        logger.debug("调试");
        logger.info("信息");
        logger.trace("结果追踪");
        logger.warn("警告");
        logger.error("错误");
        logger.fatal("致命错误？");
    }

}
