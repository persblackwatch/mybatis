package pers.xzp.mybatis;

import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import pers.xzp.mybatis.dao.UserMapper;
import pers.xzp.mybatis.pojo.User;
import pers.xzp.mybatis.utils.SqlsessionUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author : XZP
 * @version : 1.0
 * @date ：2020-09-14 22:43
 * @description : 测试类
 */
public class TestA {

    private static SqlSession sqlSession;
    static {
        sqlSession = SqlsessionUtils.getSqlSession();
    }

    @Test
    public void getSqlsessionTest() {
        System.out.println(sqlSession);
    }

    @Test
    public void getAllUser() {
        //selectList(String statement) statement是xml映射器的id
        //指向xml映射器，即DAO实现类
        Assert.assertNotNull(sqlSession.selectList("queryAllUser"));
    }

    @Test
    public void getAllUser2() {
        //mybatis推荐方式   指向接口映射器 即DAO接口
        Assert.assertNotNull(sqlSession.getMapper(UserMapper.class).queryAllUser());
    }

    @Test
    public void getOneUser() {
        Assert.assertNotNull(sqlSession.getMapper(UserMapper.class).queryOneUser(41));
    }

    @Test
    public void insertUser() {
        try {//2020-05-25 22:18:35
            User user = new User("楚毅",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) +"","男","诸天最强大佬");
            Assert.assertNotNull(sqlSession.getMapper(UserMapper.class).insertUser(user));
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
        }
    }

    @Test
    public void updateUser(){
        User user = new User();
        user.setUsername("孙悟空");
        user.setSex("男");
        user.setAddress("西游记");
        user.setId(41);
        try {
            Assert.assertNotNull(sqlSession.getMapper(UserMapper.class).updateUser(user));
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
        }
    }
    @Test
    public void deleteUser(){
        try {
            Assert.assertNotNull(sqlSession.getMapper(UserMapper.class).deleteUser(58));
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
        }
    }

    @Test public void queryAllLike(){
        Assert.assertNotNull(sqlSession.getMapper(UserMapper.class).queryAllLike("小"));
    }

    @After
    public void closeSqlSession(){
        sqlSession.close();
    }
}
