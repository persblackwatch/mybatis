package pers.xzp.mybatis.utils;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/14
 * @// TODO: Sqlsession工具类
 */
@SuppressWarnings("all")
public class SqlsessionUtils {
    private static SqlSessionFactory sqlSessionFactory;
    private static  SqlSession sqlSession ;
    static {
        InputStream inputStream = null;
        try {
            String resource = "mybatis-config.xml";
            inputStream = Resources.getResourceAsStream(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @return SqlSession
     */
    public static SqlSession getSqlSession() {
        //设置数据库开启事务
        //MySQL在mybatis中默认开启事务  即关闭MySQL自动提交数据
        return sqlSession==null?sqlSessionFactory.openSession(true):sqlSession;
    }

    /**
     * 关闭sqlSession
     * @param sqlSession
     */
    public static void close(SqlSession sqlSession){
        if (sqlSession != null) {
            sqlSession.close();
        }
    }
}
