package pers.xzp.mybatis.dao;

import pers.xzp.mybatis.pojo.User;

import java.util.List;

/**
 * @author : XZP
 * @version : 1.0
 * @date ：2020-09-14 22:51
 * @description : UserDao接口
 */
public interface UserMapper {
    List<User> queryAllUser();

    User queryOneUser(int id) ;

    int insertUser(User user);

    int updateUser(User user);

    int deleteUser(int id);

    List<User> queryAllLike(String likeKey);
}
