# mybatis学习归纳

# 问题解决
## 问题1
**描述**<br />
在代码确认无误的情况下，当然**参数带有中文**，总是不能查询到自己想要的期待结果
```sql
select * from user where id=#{id} or username=#{username}
```
单独执行sql可以查询到3条记录（id的一条，username两条），但是在程序中只能查询到1条（id的一条）

**原因**<br />
原因是`db.properties`文件中url属性的值出现问题

我们来了解一下：&（&amp;）<br />
在properties文件中不需要转义（&）<br />
在xml文件中需要转义（&amp;）<br />

我的问题产生原因就在于：在`url`属性值中使用了转义符号`&amp;`，导致`characterEncoding=utf-8`不起作用，从而导致中文乱码查不到数据
```properties
jdbc:mysql://blackwatch.top:3306/mybatis?useUnicode=true&characterEncoding=utf-8
```

**解决**<br />
将properties文件中的转义符号该为正常符号

## 事务管理
sqlSessionFactory.openSession(boolean isAuto)<br />
可以设置是否自动提交事务，若要开启事务就需要设置为false<br />
具体查看mybatis-annotation

## 待解惑
`@Param`如何使用？

## 已解惑
`@Param`用于绑定参数

## mybatis-java
基于java编程的SqlSessionFactory

## 扩展
反射+代理

利用拦截器做统一分页

回顾：过滤器，监听器 => servlet
