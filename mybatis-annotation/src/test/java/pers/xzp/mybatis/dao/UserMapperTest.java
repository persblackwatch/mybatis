package pers.xzp.mybatis.dao;

import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pers.xzp.mybatis.pojo.User;
import pers.xzp.mybatis.utils.SqlsessionUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/17
 * @// TODO: mybatis注解版本测试
 */
public class UserMapperTest {

    private static SqlSession sqlSession = SqlsessionUtils.getSqlSession(false);
    private static UserMapper userMapper;

    @Before
    public void initMapper() {
        userMapper = sqlSession.getMapper(UserMapper.class);
    }

    @After
    public void tearDown() throws Exception {
        sqlSession.close();
    }

    @Test
    public void queryAllUser() {
        userMapper.queryAllUser();
    }

    @Test
    public void queryOneUser2() {
        userMapper.queryOneUser2(41,"十号");
    }

    @Test
    public void queryOneUser() {
        userMapper.queryOneUser(59);
    }

    @Test
    public void insertUser() {
        userMapper.insertUser(new User("乌海",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) +"","男","美食供应商"));
        sqlSession.commit();
    }

    @Test
    public void updateUser() {
        User user = new User();
        user.setUsername("尘青子");
        user.setSex("男");
        user.setAddress("三寸人间");
        user.setId(60);
        userMapper.updateUser(user);
        sqlSession.commit();
    }

    @Test
    public void deleteUser() {
        userMapper.deleteUser(41);
        sqlSession.commit();
    }

    @Test
    public void queryAllLike() {
        userMapper.queryAllLike("小");
    }

    /**
     * sql分页查询
     */
    @Test
    public void queryByLimit1() {
        HashMap<String, Integer> map = new HashMap<>();
        map.put("pageIndex",0);
        map.put("pageSize",5);
        userMapper.queryByLimit1(map);
    }

    /**
     * 分页查询java层面：RowBounds
     * 原理：查询所有数据出来，然后逻辑分页
     * 若数据库量大，这种分页效率非常低
     */
    @Test
    public void queryByLimit2(){
        //RowBounds分页，只能如此查询
        //sqlSession.selectList(接口方法[全、短名都可]，方法参数[无则null]，RowBounds逻辑分页对象)
        //new RowBounds(offset,limit)
        sqlSession.selectList("queryByLimit2",null,new RowBounds(1,0));
    }
}