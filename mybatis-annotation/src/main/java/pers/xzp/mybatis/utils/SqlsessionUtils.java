package pers.xzp.mybatis.utils;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/14
 * @// TODO: Sqlsession工具类
 */
@SuppressWarnings("all")
public class SqlsessionUtils {
    private static SqlSessionFactory sqlSessionFactory;
    private static  SqlSession sqlSession ;
    static {
        InputStream inputStream = null;
        try {
            String resource = "mybatis-config.xml";
            inputStream = Resources.getResourceAsStream(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     *
     * @param isAuto 是否自动提交事务 true则自动提交数据  false则开启事务管理
     * @return SqlSession
     */
    public static SqlSession getSqlSession(boolean isAuto) {
        //设置数据库开启事务
        //openSession()：可以设置布尔参数，意在是否自动提交事务
        return sqlSession==null?sqlSessionFactory.openSession(isAuto):sqlSession;
    }

    /**
     * 关闭sqlSession
     * @param sqlSession
     */
    public static void close(SqlSession sqlSession){
        if (sqlSession != null) {
            sqlSession.close();
        }
    }
}
