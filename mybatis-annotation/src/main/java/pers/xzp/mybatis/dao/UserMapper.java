package pers.xzp.mybatis.dao;

import org.apache.ibatis.annotations.*;
import pers.xzp.mybatis.pojo.User;

import java.util.List;
import java.util.Map;

/**
 * @author : XZP
 * @version : 1.0
 * @date ：2020-09-14 22:51
 * @description : UserDao接口
 */
public interface UserMapper {
    @Select("select * from user")
    List<User> queryAllUser();

    @Select("select * from user where id=#{id}")
    User queryOneUser(int id);

    /**
     * 多个参数
     *
     * @param id
     * @param username
     * @return
     */
    @Select("select * from user where id=#{id} and username=#{username}")
    User queryOneUser2(@Param("id") int id, @Param("username") String username);

    @Insert("insert into user(username,birthday,sex,address) value(\n" +
            "          #{username},#{birthday},#{sex},#{address}\n" +
            "        )")
    int insertUser(User user);

    @Update("update user set username=#{username},sex=#{sex},address=#{address} where id=#{id}")
    int updateUser(User user);

    @Delete("delete from user where id=#{id}")
    int deleteUser(int id);

    @Select("select * from mybatis.user where username like concat('%',#{liekKey},'%')")
    List<User> queryAllLike(String likeKey);

    /**
     * 分页查询SQL层面：limit
     *
     * @param map
     * @return
     */
    @Select("select * from user limit #{pageIndex},#{pageSize}")
    List<User> queryByLimit1(Map<String, Integer> map);

    /**
     * 分页查询java层面：RowBounds
     * 原理：查询所有数据出来，然后逻辑分页
     * 若数据库量大，这种分页效率非常低
     *
     * @return
     */
    @Select("select * from user")
    List<User> queryByLimit2();
}
