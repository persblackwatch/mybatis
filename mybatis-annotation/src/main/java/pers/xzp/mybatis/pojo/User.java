package pers.xzp.mybatis.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author : XZP
 * @version : 1.0
 * @date ：2020-09-14 22:49
 * @description : s_account实体类
 */
@Data
public class User implements Serializable {
    private int id;
    private String username;
    private String birthday;
    private String sex;
    private String address;

    public User() {
    }

    public User(String username, String birthday, String sex, String address) {
        this.username = username;
        this.birthday = birthday;
        this.sex = sex;
        this.address = address;
    }
}
