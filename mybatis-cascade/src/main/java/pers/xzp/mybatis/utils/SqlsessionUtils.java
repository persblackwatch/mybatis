package pers.xzp.mybatis.utils;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/14
 * @// TODO: Sqlsession工具类
 */
@SuppressWarnings("all")
public class SqlsessionUtils {
    private static SqlSessionFactory sqlSessionFactory;
    private static  SqlSession sqlSession ;
    static {
        InputStream inputStream = null;
        try {
            String resource = "mybatis-config.xml";
            inputStream = Resources.getResourceAsStream(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 是否自动提交数据 false开启事务管理  true关闭事务管理
     * @param isAuto
     * @return SqlSession
     */
    public static SqlSession getSqlSession(boolean isAuto) {
        return sqlSession==null?sqlSessionFactory.openSession(isAuto):sqlSession;
    }

    /**
     * 关闭sqlSession
     * @param sqlSession
     */
    public static void close(SqlSession sqlSession){
        if (sqlSession != null) {
            sqlSession.close();
        }
    }
}
