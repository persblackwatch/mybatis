package pers.xzp.mybatis.pojo;

import lombok.Data;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/18
 * @// TODO: Teacher
 */
@Data
public class Teacher {
    private int id;
    private String name;
    //班主任的学生们
//    private List<Student> studentList;
}
