package pers.xzp.mybatis.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/17
 * @// TODO: Account
 */
@Data
public class Account implements Serializable {
    private int id;
    private int uid;
    private double money;
    //一个账户 => 一个用户
    private User user;

}
