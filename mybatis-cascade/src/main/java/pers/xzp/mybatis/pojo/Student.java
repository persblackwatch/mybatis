package pers.xzp.mybatis.pojo;

import lombok.Data;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/18
 * @// TODO: Student
 */
@Data
public class Student {
    private int id;
    private String name;
    //班主任 stu.tid => tea.id
    private Teacher teacher;
}
