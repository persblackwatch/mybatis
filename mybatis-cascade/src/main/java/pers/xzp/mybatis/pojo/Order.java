package pers.xzp.mybatis.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/19
 * @// TODO: 订单
 */
@Data
public class Order implements Serializable {
    private int id;
    private Date ordertime;
    private double total;
    private User user;
}
