package pers.xzp.mybatis.pojo;

import lombok.Data;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/18
 * @// TODO: AccountUser
 */
@Data
public class AccountUser extends Account {
    private String username;
    private String sex;

}
