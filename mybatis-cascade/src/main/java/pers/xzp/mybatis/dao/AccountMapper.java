package pers.xzp.mybatis.dao;

import pers.xzp.mybatis.pojo.Account;
import pers.xzp.mybatis.pojo.AccountUser;

import java.util.List;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/18
 * @// TODO:
 */
public interface AccountMapper {
    //一对一，查询用户信息
    List<AccountUser> queryuser();

    //resultMap实现一对一，查询账户对应的用户
    List<Account> queryAccoutUserMap();
}
