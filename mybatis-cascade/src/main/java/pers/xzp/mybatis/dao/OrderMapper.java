package pers.xzp.mybatis.dao;

import pers.xzp.mybatis.pojo.Order;
import pers.xzp.mybatis.pojo.User;

import java.util.List;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/19
 * @// TODO: OrderMapper
 */
public interface OrderMapper {
    /**
     * 一对一：查询订单，同时查询出订单所属哪个用户
     * @return
     */
    List<Order> queryAllOrder();

    /**
     * 一对多：查询一个用户，同时查询出该用户所有订单
     * @return
     */
    List<User> queryAllUser();
}
