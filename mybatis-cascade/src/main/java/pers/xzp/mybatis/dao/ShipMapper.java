package pers.xzp.mybatis.dao;

import org.apache.ibatis.annotations.Param;
import pers.xzp.mybatis.pojo.Student;
import pers.xzp.mybatis.pojo.Teacher;

import java.util.List;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/18
 * @// TODO:
 */
public interface ShipMapper {
    /**
     * 一对一：查询每个学生的班主任
     * @param id
     * @return
     */
    Teacher queryTeacher(@Param("tid") int id);

    /**
     *
     * @return
     */
    List<Student> queryStudents();

}
