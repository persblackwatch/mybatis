package pers.xzp.mybatis.dao;

import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pers.xzp.mybatis.pojo.AccountUser;
import pers.xzp.mybatis.utils.SqlsessionUtils;

import java.util.List;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/18
 * @// TODO:
 */
public class AccountMapperTest {

    private static SqlSession sqlSession;

    @Before
    public void initSqlSession() throws Exception {
        sqlSession = SqlsessionUtils.getSqlSession(false);
    }

    @After
    public void close() throws Exception {
        if (sqlSession != null) {
            sqlSession.close();
        }
    }

    @Test
    public void queryuser() {
        List<AccountUser> queryuser = sqlSession.getMapper(AccountMapper.class).queryuser();
        System.out.println(queryuser);
    }

    @Test
    public void queryAccoutUserMap() {
        sqlSession.getMapper(AccountMapper.class).queryAccoutUserMap();
    }
}