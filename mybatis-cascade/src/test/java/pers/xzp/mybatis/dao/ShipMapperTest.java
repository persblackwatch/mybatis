package pers.xzp.mybatis.dao;

import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pers.xzp.mybatis.utils.SqlsessionUtils;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/19
 * @// TODO:
 */
public class ShipMapperTest {

    private static SqlSession sqlSession;

    @Before
    public void initSqlSession() throws Exception {
        sqlSession = SqlsessionUtils.getSqlSession(false);
    }

    @After
    public void close() throws Exception {
        if (sqlSession != null) {
            sqlSession.close();
        }
    }

    @Test
    public void queryTeacher() {
        sqlSession.getMapper(ShipMapper.class).queryTeacher(1);
    }

    @Test
    public void queryStudents() {
    }
}