package pers.xzp.mybatis.dao;

import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pers.xzp.mybatis.utils.SqlsessionUtils;

/**
 * @author xzp
 * @version 1.0
 * @date: 2020/09/19
 * @// TODO:
 */
public class OrderMapperTest {

    private static SqlSession sqlSession;

    @Before
    public void initSqlSession() throws Exception {
        sqlSession = SqlsessionUtils.getSqlSession(false);
    }

    @After
    public void close() throws Exception {
        if (sqlSession != null) {
            sqlSession.close();
        }
    }

    @Test
    public void queryAllOrder() {
        sqlSession.getMapper(OrderMapper.class).queryAllOrder().forEach(System.out::println);
    }

    @Test
    public void queryAllUser() {
        sqlSession.getMapper(OrderMapper.class).queryAllUser().forEach(System.out::println);
    }
}